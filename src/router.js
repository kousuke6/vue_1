import Vue from 'vue'
import App from './App.vue'
import Page1 from './Page1.vue'
import Page2 from './Page2.vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/aaa',
      name: 'home',
      component: App
    },
    {
      path: '/page1',
      name: 'home',
      component: Page1
    },
    {
      path: '/page2',
      name: 'home',
      component: Page2
    },
  ]
})